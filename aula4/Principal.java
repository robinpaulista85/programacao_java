package aula4;

public class Principal {
    
    public static void main(String[] args) {
        
        for(int i = 0; i < 5; i++){
            System.out.println(i + " ");
        }

        int idade = 18;

        for(int i = idade; i < idade + 20; i++){
            System.out.println(i + " ");
        }

        int contador = 0;
        for(int linha = 0; linha < 4; linha++){
            for(int coluna = 0; coluna < 4; coluna++){
                System.out.println(">" + contador);
                contador++;
            }
        }

        System.out.println();
        for(int linha = 0; linha < 4; linha++){
            for(int coluna = 0; coluna < 4; coluna++){
                System.out.print("*");
            }
            System.out.println();
        }
    }
}